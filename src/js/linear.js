window.onload = function(){
	
    //priradi testovacie hodnoty
    $('#testing').click(function(){
        $('#rounds').val('4');
        
        $('#subst0').val('e');
        $('#subst1').val('4');
        $('#subst2').val('d');
        $('#subst3').val('1');
        $('#subst4').val('2');
        $('#subst5').val('f');
        $('#subst6').val('b');
        $('#subst7').val('8');
        $('#subst8').val('3');
        $('#subst9').val('a');
        $('#subst10').val('6');
        $('#subst11').val('c');
        $('#subst12').val('5');
        $('#subst13').val('9');
        $('#subst14').val('0');
        $('#subst15').val('7');

        if ($('#blocks').val() == 4){
            $('#perm1').val('1'); $('#perm2').val('5');
            $('#perm3').val('9'); $('#perm4').val('13');
            $('#perm5').val('2'); $('#perm6').val('6');
            $('#perm7').val('10'); $('#perm8').val('14');
            $('#perm9').val('3'); $('#perm10').val('7');
            $('#perm11').val('11'); $('#perm12').val('15');
            $('#perm13').val('4'); $('#perm14').val('8');
            $('#perm15').val('12'); $('#perm16').val('16');
            $('#master_key').val('deadbeef');
        }
        else if ($('#blocks').val() == 16){
            $('#perm1').val('1');$('#perm2').val('17');
            $('#perm3').val('33');$('#perm4').val('49');
            $('#perm5').val('2');$('#perm6').val('18');
            $('#perm7').val('34');$('#perm8').val('50');
            $('#perm9').val('3');$('#perm10').val('19');
            $('#perm11').val('35');$('#perm12').val('51');
            $('#perm13').val('4');$('#perm14').val('20');
            $('#perm15').val('36');$('#perm16').val('52');
            $('#perm17').val('5');$('#perm18').val('21');
            $('#perm19').val('37');$('#perm20').val('53');
            $('#perm21').val('6');$('#perm22').val('22');
            $('#perm23').val('38');$('#perm24').val('54');
            $('#perm25').val('7');$('#perm26').val('23');
            $('#perm27').val('39');$('#perm28').val('55');
            $('#perm29').val('8');$('#perm30').val('24');
            $('#perm31').val('40');$('#perm32').val('56');
            $('#perm33').val('9');$('#perm34').val('25');
            $('#perm35').val('41');$('#perm36').val('57');
            $('#perm37').val('10');$('#perm38').val('26');
            $('#perm39').val('42');$('#perm40').val('58');
            $('#perm41').val('11');$('#perm42').val('27');
            $('#perm43').val('43');$('#perm44').val('59');
            $('#perm45').val('12');$('#perm46').val('28');
            $('#perm47').val('44');$('#perm48').val('60');
            $('#perm49').val('13');$('#perm50').val('29');
            $('#perm51').val('45');$('#perm52').val('61');
            $('#perm53').val('14');$('#perm54').val('30');
            $('#perm55').val('46');$('#perm56').val('62');
            $('#perm57').val('15');$('#perm58').val('31');
            $('#perm59').val('47');$('#perm60').val('63');
            $('#perm61').val('16');$('#perm62').val('32');
            $('#perm63').val('48');$('#perm64').val('64');
            $('#master_key').val('deadbeefdeadbeefaaaa');
        }
    });
    
    $('#testing').click(function(){
        var pocet = parseInt($("#blocks").val())*4;
        if ((pocet != 16) && (pocet != 64)) alert('Choose the block cipher!');
    });
    
	$('#linearCrypt').click(function(){
        
        if (kontrolaVstupov()){
            $('#draw').addClass('active');
            createLinearApproximations();//VYTVORI TABULKU LINEARNYCH APROXIMACII (menu3)
            appendAllElements();//DO menu2 PRIDA VSETKY ELEMENTY

            $("#cipher-menu").hide();
            $("#main-cipher").fadeIn(500);
            $('#cipher').fadeIn(2000);

            var rounds = parseInt(document.getElementById("rounds").value);	

            var blocks = parseInt(document.getElementById("blocks").value);
            var perm = ['null'];
            for(var i = 1; i <= blocks*4; i++){
                perm.push(document.getElementById("perm"+i).value);
            }

            var zt = 'vyborne!';
            var hex = strToHex(zt);
            var pocetPartov = Math.floor(hex.length / blocks);
            var bin = hexToBin(hex);
            var pole = new Array();
            pole = division(pocetPartov,bin,blocks);

            for(var part=1; part <= pocetPartov; part++){
                $("ul.parts").append("<li id='part"+part+"' class='part'>"+part+"</li>");
                $("#cipher").append("<div id='cipher"+part+"' class='cipher' width='95%'/>");
                drawCipher(part, rounds, blocks, perm);
                $('.part').click(function(){
                    var part = $(this).text();
                    $('.cipher').hide();
                    if (part.length == 1) $('#cipherinfo').hide();
                    $('#cipher'+part+'').fadeIn(200);
                    $(".part.active").toggleClass("active");
                    $('#part'+part+'').toggleClass("active");
                });
            }
        }
	});
}

function kontrolaVstupov(){
    var pocet = parseInt($("#blocks").val())*4;
    var k = $('#master_key').val();
    if ((pocet != 16) && (pocet != 64)) {alert('Please, choose the type of the cipher!'); return false;}
    for(var i = 0; i < 16; i++){
        var s = $('#subst'+i).val();
        if((s == '') || (s.charCodeAt(0) < 48) || (s.charCodeAt(0) > 102) || ((s.charCodeAt(0) > 57) && (s.charCodeAt(0) < 97)))
            {alert('Unknown value '+s+'... Please, set the substitution correctly!'); return false;};
    }
    for(var i = 0; i < k.length; i++){
        if((k[i] == '') || (k[i].charCodeAt(0) < 48) || (k[i].charCodeAt(0) > 102) || ((k[i].charCodeAt(0) > 57) && (k[i].charCodeAt(0) < 97)))
            {alert('Please, set the master key correctly! Input must contain only hex characters, for SPN 8 chars, for PRESENT 20!'); return false;};
    }
    for(var i = 1; i <= pocet; i++){
        var p = $('#perm'+i).val();
        if((p == '') || (parseInt(p) > pocet) || (p.charCodeAt(0) > 102) || (p.charCodeAt(0) > 57))
            {alert('Please, set the permutation '+i+' correctly!'); return false;};
    }
    
    if(((pocet == 16) && ($('#master_key').val().length != 8)) || ((pocet == 64) && ($('#master_key').val().length != 20))){
            alert('Please, set the master key correctly!'); 
            return false;
    }
        
    return true;
}

function drawCipher(part,rounds,blocks,perm){
	$('#cipher'+part).hide();
    var sirka_div = $('#cipher').width()*0.9;
    var last_subkey = $('#last_subkey').val();
    
    if (blocks == 16){
        $.post('./includes/present.php',
            { kola: rounds, subkey: last_subkey, sirka: sirka_div, bloky: blocks, permutacia: perm}, 
            function ( data ) {
                $(data).appendTo('#cipher'+part);
            });
        $.get( "./txt/present.txt", function( data ) {
          $( "#cipherinfo" ).html( data );
        });
    }
    else if (blocks == 4){
        $.post('./includes/spn.php',
            { kola: rounds, subkey: last_subkey, sirka: sirka_div, bloky: blocks, permutacia: perm}, 
            function ( data ) {
                $(data).appendTo('#cipher'+part);
            });
        $.get( "./txt/spn.txt", function( data ) {
            $( "#cipherinfo" ).html( data );
        });
    }
}


//VYTVORI LINEARNE APROXIMACIE -> menu3
function createLinearApproximations(){
	var subst = '';
    for(var i = 0; i < 16; i++){
        subst += $('#subst'+i).val();
    }
	$.post('./includes/lin-approx.php',
		{ substitucia : subst },
		function (data){
			$(data).appendTo('#lin-approx');
		});
}

//FUNKCIA PRIRADI JEDNOTLIVYM POLOZKAM V menu2 ELEMENTY
function appendAllElements(){
	$('#lin-expression').append('<h2>Linear expression</h2><p id="linear_expression"></p>').hide();
	$('#lin-expression').append('<h3>Sum of the subkeys</h3><p id="sum_subkeys"></p>').hide();
	$('#lin-expression').append('probability (if ∑k = 0) = <p id="upper_prob1"></p>').hide();
	$('#lin-expression').append('probability (if ∑k = 1) = <p id="upper_prob2"></p>').hide();
	$('#lin-expression').append('->  bias = <span style="color:red" id="upper_bias"></span> ').hide();
}

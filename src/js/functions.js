//pomocne funkcie
function make1darray(twodArray){
	var oned = new Array();
	for(var i = 0; i < twodArray.length;i++){
		for(var j = 0; j < twodArray[i].length;j++){
			oned.push(twodArray[i][j]);
		}
	}
	return oned;
}

function createSubkey(key,way){
	var rounds = parseInt(document.getElementById("rounds").value);
	var subkey = Array();
	for(var i = 0; i < rounds+1; i++){
		subkey.push(rotate(key,way))
		key = subkey[i];
	}
	return subkey;
}

function rotate(bin,way){
	var new_bin = '';
	if (way == '1'){
		new_bin += bin[bin.length-1];
		for(var i = 1; i < bin.length; i++){
			new_bin += bin[i-1];
		}
	}
	else if(way == '0'){
		for(var i = 1; i < bin.length; i++){					
			new_bin += bin[i];
		}
		new_bin += bin[0];
	}
	return new_bin;
}

function subst(sub, block){
	var lookupTable = {
		'0': sub[0], '1': sub[1], '2': sub[2], '3': sub[3], '4': sub[4],
		'5': sub[5], '6': sub[6], '7': sub[7], '8': sub[8], '9': sub[9],
		'a': sub[10], 'b': sub[11], 'c': sub[12], 'd': sub[13],
		'e': sub[14], 'f': sub[15]
	};
	if (lookupTable.hasOwnProperty(block)) {
			return lookupTable[block];
		} else {
			return { valid: false };
		}
}

function desubst(sub){
	var str = '';
	var desub = new Array;
	var pole = {a:"10", b:"11", c:"12", d:"13", e:"14", f:"15"};
	var pole2 = {10:"a", 11:"b", 12:"c", 13:"d", 14:"e", 15:"f"};
	for(var i = 0; i < 10; i++){
		if (sub[i] == 'a' || sub[i] == 'b' || sub[i] == 'c' || sub[i] == 'd' || sub[i] == 'e' || sub[i] == 'f'){
			desub[pole[sub[i]]] = i;
		}else{
			desub[sub[i]] = i;
		}
	}
	for(var i = 10; i < 16; i++){
		if (sub[i] == 'a' || sub[i] == 'b' || sub[i] == 'c' || sub[i] == 'd' || sub[i] == 'e' || sub[i] == 'f'){
			desub[pole[sub[i]]] = pole2[i];
		}else{
			desub[sub[i]] = pole2[i];
		}
	}
	for(var i = 0; i < 16; i++){
		str+=desub[i];
	}
	return str;
}

function permut(perm, block){
	var temp = '';
	for(var i = 0; i < block.length; i++){
		temp += block[parseInt(perm[i])-1];
	}
	return temp;
}

function division(pocetPartov,bin,blocks){
	var pole = new Array();
	for(var i=0; i < pocetPartov; i++){
		pole[i] = bin.substring(0,blocks*4);
		bin = bin.substring(blocks*4,bin.length);
	}
	return pole;
}

function resize(e){
	var height = $(e).parent().height();
	var width =	$(e).parent().width();
	height -= 30;
	width -= 30;
	$(e).css('min-height',''+height+'');
	$(e).css('min-width',''+width+'');
}

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode < 48 || charCode > 49)
		return false;
	return true;
}

function checkLength(){
	var pocet = parseInt($("#blocks").val())*4;
    if (pocet == 16)
        $("#master_key").attr("maxLength", 8);
    else if (pocet == 64)
        $("#master_key").attr("maxLength", 20);

    $('#parameters').html('');
    $.post('./includes/parameters.php',
           { pocet: pocet },
    function ( data ) {
        $(data).appendTo('#parameters');
    });
}


function markActive(id){
    $('.typ-sifry').each(function(){
        $(this).removeClass('typ-active');
    });
//    $('#'+id).addClass('typ-active');
}


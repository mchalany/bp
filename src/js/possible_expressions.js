$(window).ready(function(){
    
    var trajectories = new Array();
    
    $('#trajs_button').click(function(){
        var poss = createPossibleExpressions();
        var trajectories = getPossibleTrajectories(poss);
        var rounds = parseInt(document.getElementById("rounds").value);
        if ($('.cipher:visible').attr('id') == 'cipherinfo') alert('Choose the part of the cipher!');
        else{
            if (rounds <= 4){
                var current = $('.cipher:visible').attr('id');
                    $(this).hide();
                    var txt = $(this).text()
                    for(var i = 0; i < 20; i++){
                        $('.trajectories').append('<li id="td'+i+'" class="trajectory">'+(i+1)+'</li>');
                    }

                    $('.trajectories').find('li').click(function(){
                            $(".trajectory.active").toggleClass("active");
                            $(this).addClass("active");
                            markTrajectory(trajectories,$(this).text());
                            $('#cb').text('Current bias: '+trajectories[$(this).text()][0]);
                    })
            }
            else alert('For more than 4 rounds the trajectory must be marked manually!');
        }
    });
});

//na zaklade vybratej trajektorie zaznaci S-Boxy
function markTrajectory(trajs, id){
    var current = $('.cipher:visible').attr('id');
    $('#obtain').addClass('available');
    var rounds = parseInt(document.getElementById("rounds").value);
    if (rounds > 4) alert('For more than 4 rounds the trajectories must be marked manually!');
//    var final = new Array();
    var prvy = new Array();
    var t = trajs[id-1];
    var b = t[0];
    var w = parseInt(t[1]);
    var next = new Array();
    //prve kolo
    prvy = t[2].split(',');
    for(var i = 0; i < prvy.length; i++){
        if ((prvy[i][0] == 'Y') && (next[0] == null)) next[0] = (parseInt(prvy[i].slice(1))+w*4);
        else if (prvy[i][0] == 'Y') next[1] = (parseInt(prvy[i].slice(1))+w*4);
        prvy[i] = prvy[i][0] + '1,' + (parseInt(prvy[i].slice(1))+w*4);
        $("#"+current+" td")
            .filter(function() { return $.text([this]) == prvy[i]; })
            .click();
    }    
    if (rounds == 2) return 1;
    
    //druhe kolo
    prvy = new Array();
    var dalsi = new Array();
    if (t[3].length == 1) {
        var w = parseInt(t[3][0][0]);
        prvy = t[3][0][1].split(',');
        for(var i = 0; i < prvy.length; i++){
            if ((prvy[i][0] == 'Y') && (dalsi[0] == null)) dalsi[0] = (parseInt(prvy[i].slice(1))+w*4);
            else if (prvy[i][0] == 'Y') dalsi[1] = (parseInt(prvy[i].slice(1))+w*4);
            prvy[i] = prvy[i][0] + '2,' + (parseInt(prvy[i].slice(1))+(getBlock(next[0])-1)*4);
            $("#"+current+" td")
                .filter(function() { return $.text([this]) == prvy[i]; })
                .click();
        }
    }
    else if (t[3].length == 2){        
        for(var k = 0; k < next.length; k++){
            var w = parseInt(t[3][k][0]);
            prvy = t[3][k][1].split(',');
            for(var i = 0; i < prvy.length; i++){
                if ((prvy[i][0] == 'Y') && (dalsi[0] == null)) dalsi[0] = (parseInt(prvy[i].slice(1))+w*4);
                else if ((prvy[i][0] == 'Y') && (dalsi[1] == null)) dalsi[1] = (parseInt(prvy[i].slice(1))+w*4);
                else if (prvy[i][0] == 'Y') dalsi[2] = (parseInt(prvy[i].slice(1))+w*4);
                prvy[i] = prvy[i][0] + '2,' + (parseInt(prvy[i].slice(1))+(getBlock(next[k])-1)*4);
                $("#"+current+" td")
                    .filter(function() { return $.text([this]) == prvy[i]; })
                    .click();
            }
        }
    }
    if (rounds == 3) return 1;
    
    //tretie kolo
    prvy = new Array();
    for(var k = 0; k < dalsi.length; k++){
        prvy = t[4][k].split(',');
        for(var i = 0; i < prvy.length; i++){
            prvy[i] = prvy[i][0] + '3,' + (parseInt(prvy[i].slice(1))+(getBlock(dalsi[k])-1)*4);
            $("#"+current+" td")
                .filter(function() { return $.text([this]) == prvy[i]; })
                .click();
        }
    }
}

function getBlock(n){
    var result;
    var blocks = parseInt(document.getElementById("blocks").value);
    var perm = ['null'];
    for(var i = 1; i <= blocks*4; i++){
        perm.push(document.getElementById("perm"+i).value);
    }
    result = Math.ceil(perm[n]/4);
    return result;
}

//VRACIA V POLI VŠETKY MOŽNÉ TRAJEKTÓRIE VYHOVUJ/CE PODMIENKE
function getPossibleTrajectories(poss){
    var rounds = parseInt(document.getElementById("rounds").value);
    var pocet = poss.length;
    var trajs = new Array();
    var sum = new Array();
    var perm = new Array();
    var blocks = parseInt(document.getElementById("blocks").value);
    for(var i = 0; i < blocks*4; i++){
        perm.push(document.getElementById("perm"+(i+1)).value);
    }
    var permutacia = new Array();
    //1. krok, podla poctu blokov vytvorim pre kazdy blok pole moznosti s oznacenim bloku, na zaklade ktoreho zistim do ktoreho bloku idem
    for(var i = 0; i < blocks; i++){
        trajs[i] = new Array();
        permutacia[i] = [perm[i*4], perm[i*4+1], perm[i*4+2], perm[i*4+3]];
        for(var j = 0; j < pocet; j++){
            var pole = new Array();
            pole.push(poss[j][0]);
            pole.push(poss[j][1]);
            pole.push(getFollowing(poss[j][0],permutacia[i]));
            trajs[i].push(pole);
        }
    }
    
    //2. krok, vzdy vezmem jednu hodnotu z kazdej trajs a na zaklade ulozenej nasledujucej dosadim vsetky vyhovujuce, pricom vzdy kazdy dalsi vyhovujuci obsahuje aj nasledujuce
    var pocet_trajs = 0;
    for(var i = 0; i < trajs.length;i++){
        pocet_trajs += trajs[i].length;
    }
    for(var w = 0; w < blocks; w++){
        for(var i = 0; i < trajs[w].length; i++){
            var current = trajs[w][i][0];
            var bias = trajs[w][i][1];
            var next = trajs[w][i][2];
            
            var pole = new Array();
            
            if (next.length == 1){
                for(var j = 0; j < pocet; j++){
                    var x = 0;
                    var t = poss[j][0]; var bi = poss[j][1];
                    for(var z = 0; z < t.length; z++){
                        if (t[z] == 'X') x++;
                    }
                    if (((t.indexOf(next[0][1])) > -1) && (x == 1)){
                        pole[0] = pillingUpLemma([bias/16,bi/16]);
                        pole[1] = w;
                        pole[2] = current;
                        pole[3] = [[next[0][0],t]];
                        pole[4] = getFollowing(t,permutacia[next[0][0]]);
                        if (sum.indexOf(pole) < 0)
                            sum.push(pole);
                    }
                }
            }
            else if (next.length == 2){
                for(var j = 0; j < pocet; j++){
                    var x = 0;
                    var t = poss[j][0]; var bit = poss[j][1];
                    for(var z = 0; z < t.length; z++){
                        if (t[z] == 'X') x++;
                    }
                    if (((t.indexOf(next[0][1])) > -1) && (x == 1)){
                        for(var j = 0; j < pocet; j++){
                            var x = 0;
                            var r = poss[j][0]; var bir = poss[j][1];
                            for(var z = 0; z < r.length; z++){
                                if (r[z] == 'X') x++;
                            }
                            if (((r.indexOf(next[1][1])) > -1) && (x == 1)){
                                pole[0] = pillingUpLemma([bias/16,bit/16,bir/16]);
                                pole[1] = w;
                                pole[2] = current;
                                pole[3] = [[next[0][0],t],[next[1][0],r]];
                                pole[4] = getFollowing(t,permutacia[next[0][0]]).concat(getFollowing(r,permutacia[next[1][0]]));
                                if (sum.indexOf(pole) < 0)
                                    sum.push(pole);
                            }
                        }
                    }
                }
            }
        }
    }
    
    var final = new Array();
    var sum_dlzka = sum.length;
    for(var i = 0; i < sum_dlzka; i++){
        var current = sum[i][4];
        if (current.length == 2){
            var pole = new Array();
            for(var j = 0; j < pocet; j++){
                var x = 0;
                var r = poss[j][0]; var bir = poss[j][1];
                for(var z = 0; z < r.length; z++){
                    if (r[z] == 'X') x++;
                }
                if (((r.indexOf(current[0][1])) > -1) && (x == 1)){
                    for(var k = 0; k < pocet; k++){
                        var x = 0;
                        var t = poss[k][0]; var bit = poss[k][1];
                        for(var n = 0; n < t.length; n++){
                            if (t[n] == 'X') x++;
                        }
                        if (((t.indexOf(current[1][1])) > -1) && (x == 1)){
                            pole[0] = pillingUpLemma([sum[i][0],bir/16,bit/16]);
                            pole[1] = sum[i][1];
                            pole[2] = sum[i][2];
                            pole[3] = sum[i][3];
                            pole[4] = [r,t];
                            if (final.indexOf(pole) < 0)
                                final.push(pole);
                        }
                    }
                }
            }
        }
        else if (current.length == 3){
            var pole = new Array();
            for(var j = 0; j < pocet; j++){
                var x = 0;
                var r = poss[j][0]; var bir = poss[j][1];
                for(var z = 0; z < r.length; z++){
                    if (r[z] == 'X') x++;
                }
                if (((r.indexOf(current[0][1])) > -1) && (x == 1)){
                    for(var k = 0; k < pocet; k++){
                        var x = 0;
                        var t = poss[k][0]; var bit = poss[k][1];
                        for(var n = 0; n < t.length; n++){
                            if (t[n] == 'X') x++;
                        }
                        if (((t.indexOf(current[1][1])) > -1) && (x == 1)){
                            for(var l = 0; l < pocet; l++){
                                var x = 0;
                                var s = poss[l][0]; var bis = poss[l][1];
                                for(var m = 0; m < s.length; m++){
                                    if (s[m] == 'X') x++;
                                }
                                if (((s.indexOf(current[2][1])) > -1) && (x == 1)){
                                    pole[0] = pillingUpLemma([sum[i][0],bir/16,bit/16,bis/16]);
                                    pole[1] = sum[i][1];
                                    pole[2] = sum[i][2];
                                    pole[3] = sum[i][3];
                                    pole[4] = [r,t,s];
                                    if (final.indexOf(pole) < 0)
                                        final.push(pole);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    final = final.sort().reverse();
    return final;
}

//vezme vyraz a na zaklade permutacie oznaci casti vyrazu ktore musi obsahovat nasledujuci blok
function getFollowing(exp,perm){
    var following = new Array();
    var temp = exp.split(',');
    var pole = new Array();
    for(var i = 0; i < temp.length; i++){
        if (temp[i].indexOf('Y') > -1) pole.push(temp[i]);
    }
    for(var i = 0; i < pole.length; i++){
        var tmp = parseInt(perm[parseInt(pole[i].substr(1))-1]);
        var p = tmp % 4;
        if (p == 0) p = 4;
        following.push([Math.ceil(tmp/4)-1,'X'+p]);
        if (following.length == 2) break;
    }
    return following;
}

//ZOBRAZI VSETKY MOZNE KOMBINACIE S-BOXOV DO TABULKY
function createPossibleExpressions(){
    var possible = make1darray(getPossibleExpressions());
    var poss = new Array();
    var bias = new Array();
    var spolu = new Array();
    for(var i = 0; i < possible.length; i++){
        var y = 0;
        for(var j = 0; j < possible[i].length; j++){
            if (possible[i][j][0] == 'Y') y++;
        }
        if(y <= 2){
            var b = Math.abs(getBias(makeProb(possible[i])));
            bias.push(b);
            var temp = '';
            for(var j = 0; j < possible[i].length; j++){
                temp+= possible[i][j];
                if(j != possible[i].length-1)temp+= ',';
            }
            poss.push(temp);
            spolu.push([temp,b]);
        }
    }

    $('#possible-expression').html("");
    
    $.ajax({
        type: 'POST',
        url:'./includes/possible-expression.php',
        data: { possible : poss, bias : bias },
        success: function ( data ) {
            $(data).appendTo('#possible-expression');
        }
    });
    return spolu;
}

function makeProb(exp){
    var lookupTable = {
        'X1': '0', 'X2': '1', 'X3': '2', 'X4': '3', 
        'Y1': '4', 'Y2': '5', 'Y3': '6', 'Y4': '7'
    }; 

    var table = new Array();
    table = getApproximations();

    var xorArray = new Array;
    for(var k = 0; k < exp.length; k++){
        xorArray.push(table[lookupTable[exp[k]]]);
    }
    var count = 0;
    for(var i = 0; i < 16; i++){
        var result = xorArray[0][i];
        for(var j = 1; j < xorArray.length; j++){
            result = xor(result,xorArray[j][i]);
        }
        if(result == 0){
            count++;
        }
    }

    return count;
}

function getPossibleExpressions(){
    var possible = new Array();
    for(var i = 2; i < 8; i++){
        possible.push(ugly(i));
    }
    return possible;
}

//NAJSKAREDSIA FUNKCIA V HRE
//vracia vsetky kombinacie 
function ugly(i){
    var e = new Array();
    $('#table-approx th').each(function(){
        e.push($(this).text());
    });
    var one = new Array();
    var probs = new Array();
    if(i==2){
        for(var j = 0; j < 7; j++){
            for(var i = j+1; i < 8; i++){		
                one.push(e[j]);
                one.push(e[i]);
                if (makeProb(one) != 8){
                    probs.push(one)
                }
                one = [];
            }
        }
        return probs;
    }
    else if(i==3){
        for(var k = 0; k < 6; k++){
            for(var j = k+1; j < 7; j++){
                for(var i = j+1; i < 8; i++){	
                    one.push(e[k]);
                    one.push(e[j]);
                    one.push(e[i]);
                    if (makeProb(one) != 8){
                        probs.push(one);
                    }
                    one = [];
                }
            }
        }	
        return probs;
    }
    else if(i==4){
        for(var l = 0; l < 5; l++){
            for(var k = l+1; k < 6; k++){
                for(var j = k+1; j < 7; j++){
                    for(var i = j+1; i < 8; i++){	
                        one.push(e[l]);
                        one.push(e[k]);
                        one.push(e[j]);
                        one.push(e[i]);
                        if (makeProb(one) != 8){
                            probs.push(one);
                        }
                        one = [];
                    }
                }
            }	
        }
        return probs;
    }
    else if(i==5){
        for(var m = 0; m < 4; m++){
            for(var l = m+1; l < 5; l++){
                for(var k = l+1; k < 6; k++){
                    for(var j = k+1; j < 7; j++){
                        for(var i = j+1; i < 8; i++){	
                            one.push(e[m]);
                            one.push(e[l]);
                            one.push(e[k]);
                            one.push(e[j]);
                            one.push(e[i]);
                            if (makeProb(one) != 8){
                                probs.push(one);
                            }
                            one = [];
                        }
                    }
                }	
            }
        }
        return probs;
    }
    else if(i==6){
        for(var n = 0; n < 3; n++){
            for(var m = n+1; m < 4; m++){
                for(var l = m+1; l < 5; l++){
                    for(var k = l+1; k < 6; k++){
                        for(var j = k+1; j < 7; j++){
                            for(var i = j+1; i < 8; i++){	
                                one.push(e[n]);
                                one.push(e[m]);
                                one.push(e[l]);
                                one.push(e[k]);
                                one.push(e[j]);
                                one.push(e[i]);
                                if (makeProb(one) != 8){
                                    probs.push(one);
                                }
                                one = [];
                            }
                        }
                    }	
                }
            }
        }
        return probs;
    }
    else if(i==7){
        for(var o = 0; o < 2; o++){
            for(var n = o+1; n < 3; n++){
                for(var m = n+1; m < 4; m++){
                    for(var l = m+1; l < 5; l++){
                        for(var k = l+1; k < 6; k++){
                            for(var j = k+1; j < 7; j++){
                                for(var i = j+1; i < 8; i++){	
                                    one.push(e[o]);
                                    one.push(e[n]);
                                    one.push(e[m]);
                                    one.push(e[l]);
                                    one.push(e[k]);
                                    one.push(e[j]);
                                    one.push(e[i]);
                                    if (makeProb(one) != 8){
                                        probs.push(one);
                                    }
                                    one = [];
                                }
                            }
                        }	
                    }
                }
            }
        }
        return probs;
    }
}

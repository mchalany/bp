function hexToStr(hex){
	var hex = hex.toString();
	var str = '';
	for (var i = 0; i < hex.length; i += 2)
		str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
	return str;
}

function strToHex(str){
	var hex = '';
	for(var i=0;i<str.length;i++) {
		hex += ''+str.charCodeAt(i).toString(16);
	}
	return hex;
}

function hexToDec(hex){
    return parseInt(hex,16);
}

//function decToHex(dec){
//    return dec.toString(16);
//}

function hexToBin(s) {
	var ret = '';
	var lookupTable = {
		'0': '0000', '1': '0001', '2': '0010', '3': '0011', '4': '0100',
		'5': '0101', '6': '0110', '7': '0111', '8': '1000', '9': '1001',
		'a': '1010', 'b': '1011', 'c': '1100', 'd': '1101',
		'e': '1110', 'f': '1111',
	};
	for (var i = 0; i < s.length; i += 1) {
		if (lookupTable.hasOwnProperty(s[i])) {
			ret += lookupTable[s[i]];
		} else {
			return { valid: false };
		}
	}
	return ret;
}

function binToHex(bin){
	var ret = '';
	var lookupTable = {
		'0000': '0', '0001': '1', '0010': '2', '0011': '3', '0100': '4',
		'0101': '5', '0110': '6', '0111': '7', '1000': '8', '1001': '9',
		'1010': 'a', '1011': 'b', '1100': 'c', '1101': 'd',
		'1110': 'e', '1111': 'f'
	};
	if (lookupTable.hasOwnProperty(bin)) {
		return lookupTable[bin];
	} else {
		return { valid: false };
	}
	return ret;
}

function xor(a,b){
	var str = '';
	if (a.length != b.length) return false;
	else{
		for(var i = 0; i < a.length; i++){
			str+=a[i]^b[i];
		}
	}
	return str;
}

Date.prototype.timeNow = function () {
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}
Date.prototype.today = function () { 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}
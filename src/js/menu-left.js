//este spravit: PO KLIKNUTI NA SIPKU SA ZMENI OPACITY NA ELEMENTOCH CONTAINER-CIPHER OKREM MENU SAMOZREJME
	$(window).ready(function(){
		//ANIMACIA LAVEHO MENU
		$('.menu-1').on({
			mouseenter: function(){$(this).css('backgroundColor','white');
            $(this).css('color','black');},
			mouseleave: function(){$(this).css('backgroundColor','black');
            $(this).css('color','white');}
		});
        $('.menu-2').on({
			mouseenter: function(){$(this).css('backgroundColor','white');
            $(this).css('color','black');},
			mouseleave: function(){$(this).css('backgroundColor','navy');
            $(this).css('color','white');}
		});
		$('.menu').click(function(){
				$(this).css('backgroundColor','green');
		});
		$('#menu-button').click(function(){
            if($('#menu-left ul').css('display') == 'none'){
                $(this).text('⇐');
                $('#menu-left').animate({
                    width:'15%'
                },200);
                $('#menu-left ul').show();
            }
            else{
                $(this).text('⇒');
                $('#menu-left ul').hide();
                $('#menu-left').animate({
                    width:'1%'
                },200);
            }
		});
		//KONIEC ANIMACIE LAVEHO MENU
		
        //docasne aj menu up
        $('#draw').click(function(){
            if (!($(this).hasClass('disabled')) && !($(this).hasClass('active'))){
                $(this).addClass('active');
                $('#container-main').children().hide();
                $('#cipher-parameters').fadeIn(500);     
            }
        });
        
        $('#obtain').click(function(){
            var r = $('#rounds').val();
            var biasy = new Array();
            if(checkCompleteness(r)){
                $(this).removeClass('available');
                $('#draw').removeClass('active');
                $('#draw').addClass('disabled');
                
                var current = $('.cipher:visible').attr('id');
                var oznacene = getSelectedBoxes(current);
                for(var i = 1; i <= oznacene.length; i++){
                    var currentExp = $("#expression"+i+"").text();
                    if (currentExp != oznacene[i-1]){
                        var prob = getProbability(oznacene[i-1]);
                    }
                    biasy.push(getBias(prob)/16);
                }
                
                $('#examples_info').text('Recommended number of samples: '+Math.pow(pillingUpLemma(biasy),-2));
                if (!($(this).hasClass('disabled')) && !($(this).hasClass('active'))){
                    $(this).addClass('active');
                    $('#draw').removeClass('active');
                    $('#draw').addClass('disabled');
                    $('#container-main').children().hide();
                    $("#examples").fadeIn(500);
                }
            }
            else{
                alert('Wrong trajectory!');
            }
        });
        
        $('#applylc').click(function(){
            if ($(this).hasClass('available')) {
                $('#draw').addClass('disabled');
                $(this).removeClass('available');  
                $(this).addClass('active');
                $('#obtain').removeClass('active');
                $('#obtain').addClass('disabled');
                $('#container-main').children().hide();
                $("#final").fadeIn(500);
            }
            else{
                alert('You missed some of the previous steps!');
            }
        });
        //KONIEC MENU UP teda NAV
        
        //ZOBRAZENIE HOME
		$('#menu0').click(function(){
            window.location.href="index.php";
			$('#container-main').children().hide();
			$('#home').show();
        });
        
        $('#accordion').click(function(){
            if ($('#acc-panel').css('display') == 'none')
            $(this).text('Cipher tools «');
            else 
            $(this).text('Cipher tools »');
            $('#acc-panel').slideToggle();
        });
        
		//ZOBRAZENIE SIFRY -> MOZNOST NAKLIKAT TRAJEKTORIU
		$('#menu1').click(function(){
			$('#container-main').children().hide();
            $("#main-cipher").fadeIn(500);
        });
        
		//ZOBRAZENIE VYRAZOV PRE JEDNOTLIVE S-BOXy + KONECNY LINEARNY VYRAZ + BIAS
		$('#menu2').click(function(){
			var current = $('.cipher:visible').attr('id');
            var oznacene = getSelectedBoxes(current);
			$('#upper_prob1').empty();
			$('#upper_prob2').empty();
			$('#upper_bias').empty();
			for(var i = 1; i <= oznacene.length; i++){
				$('#menu-bottom-left').append('<p id="expression'+i+'"></p>');//.empty()
				$('#menu-bottom-right').append('<p id="bias'+i+'"></p>');
			}
            
			createExpressions(current,oznacene);
			
            $('#container-main').children().hide();
			$("#menu-bottom").fadeIn(500);
			$("#lin-expression").fadeIn(500);
		});
        
		//ZOBRAZENIE LINEARNYCH APROXIMACII DANEJ SUBSTITUCIE
		$('#menu3').click(function(){
			$('#container-main').children().hide();
			$("#lin-approx").fadeIn(500);
		});
        
		//ZOBRAZENIE KOMBINACII VYRAZOV
		$('#menu4').click(function(){
			$('#container-main').children().hide();
			$("#possible-expression").fadeIn(500);
			$("#possible").fadeIn(500);
		});
        
		//ZOBRAZENIE TABULKY SUBKEYS
		$('#menu5').click(function(){
			//var current = $('.cipher:visible').attr('id');
			//extractKeyBits();
			$('#container-main').children().hide();
			$("#examples").fadeIn(500);      
            $('#examples-header').hide();
		});
        
        $('#menu6').click(function(){
			$('#container-main').children().hide();
			$("#final").fadeIn(500);
            $('#fin-header').hide();
		});
		
        //VYMAZANIE PRAVE ZOBRAZENEHO PARTU SIFRY (1 sifra)
		$('#menu7').click(function(){
            $('#obtain').removeClass('available');
            removeTrajectory();
		});
        
		$('#menu8').click(function(){
            $('#container-main').children().hide();
            $('#model').fadeIn(500);
            $('#model_header').fadeIn(500);
            $('#model_container').fadeIn(500);
		});
		
        
	});

function removeTrajectory(){
    var pocet = parseInt($("#blocks").val())*4;
    var last_subkey = $('#last_subkey').val();
    var rounds = parseInt(document.getElementById("rounds").value);
    var blocks = parseInt(document.getElementById("blocks").value);
    var sirka_div = $('#cipher').width()*0.9;
    var perm = ['null'];
    for(var i = 1; i <= blocks*4; i++){
        perm.push(document.getElementById("perm"+i).value);
    }
    //vymazat celu nakreslenu sifru a nakreslit znova!
    var current = $('.cipher:visible').attr('id');
    $('#'+current).find('.td-active')
        .removeClass('td-active')
        .addClass('td-inactive');
    $('#'+current).find('.td-active-subkey').removeClass('td-active-subkey');
    $('#'+current).find('.td-target-subkey').removeClass('td-target-subkey');
    
    if (current == 'cipherinfo'){
        alert('Cannot remove info subsite! No trajectories here...');
    }
    else{
        $('#'+current).html('');
        if (pocet == 64){
            $.post('./includes/present.php',
                { kola: rounds, subkey: last_subkey, sirka: sirka_div, bloky: blocks, permutacia: perm}, 
                function ( data ) {
                    $(data).appendTo('#'+current);
            });
        }
        else if (pocet == 16){
            $.post('./includes/spn.php',
                { kola: rounds, subkey: last_subkey, sirka: sirka_div, bloky: blocks, permutacia: perm}, 
                function ( data ) {
                    $(data).appendTo('#'+current);
            });
        }
    }
    
    $('#upper_prob1').empty();
    $('#upper_prob2').empty();
    $('#upper_bias').empty();
    $('#menu-bottom-right').empty();
    $('#menu-bottom-left').empty();
}

//pozera vsetky aktivne TDcka a ak je v kazdom kole oznacene aspon jedno tak umozni ist do dalsieho bodu
function checkCompleteness(rounds){
    var aktivne = new Array();
    var bool = false;
    var c = $('.cipher:visible').attr('id');
    $('#'+c+' .td-active').each(function(){
        aktivne.push($(this).text());
    })
    for(var i = 1; i < rounds; i++){
        for(var j = 0; j < aktivne.length; j++){
            if (i == (rounds - 1)){
                if(((parseInt(aktivne[j][1])) == i) && (aktivne[j][0] == 'Y')){
                    bool = true;
                    break;
                }
                else bool = false;
            }
            else if((parseInt(aktivne[j][1])) == i) {
                bool = true; 
                break;
            }
            else bool = false;
        }
    }
    if (bool == false) return 0;
    else return 1;
}
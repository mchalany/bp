$(window).ready(function(){
    var subkeys = new Array();
    var rounds = 4;//ziskanie poctu kol dorobit!!!
    
    $('#key-schedule').click(function(){
        if (($('#subkey0').length)){
            alert('Subkeys already generated!');    
        }
        else{
            var master = hexToBin($('#master_key').val());
            var pocetBitov = $('#blocks').val()*4;
            if (pocetBitov == 16)
                subkeys = spnKeySchedule(master,rounds);
            else if (pocetBitov == 64)
                subkeys = presentKeySchedule(master,rounds);
            for(var i = 0; i <= rounds; i++){
                $('#subkeys ul').append('<li id="subkey'+i+'">'+subkeys[i]+'</li>');
            }
            var sub = '';
            for(var i = 0; i < pocetBitov / 4; i++){
                sub += binToHex(subkeys[rounds].substring(i*4,i*4+4));
            }
            $('#last_sub').text('Last-subkey: '+sub);
        }
    });
    
    $('#samplesOT').click(function(){ 
        if (($('#ot0').length)){
            alert('Subkeys already generated!');    
        }
        else{
            var pocetBitov = $('#blocks').val()*4;
            //vytvori do MENU5 nalavo vzorky OT
            createPlaintexts(pocetBitov);
        }
    });
    
    $('#samplesZT').click(function(){
        if(!($('#ot0').length) || !($('#subkey0').length))
            alert('Please, create subkeys and OT samples first!');
        else if (($('#zt0').length)){
            alert('Samples of ZT already generated!');
        }
        else{
            $('#applylc').addClass('available');              
            var pocetBitov = $('#blocks').val()*4;
            var rounds = $('#rounds').val();

            var subst = '';
            for(var i = 0; i < 16; i++){
                subst += $('#subst'+i).val();
            }
            var last_subkey = subkeys[rounds];
            var pocet_vzoriek = $('#pocet_vzoriek').val();
            var perm = ['null'];
            for(var i = 1; i <= pocetBitov; i++){
                perm.push($("#perm"+i+"").val());
            }
            //zo vzoriek OT do MENU5 napravo vytvori vzorky ZT
            //vezme jednu vzorku OT z laveho a spravi ZT do praveho
            for(var i = 0; i < pocet_vzoriek; i++){//10000
                var ot = $('#ot'+i).text();
                var zt = xor(ot,subkeys[0]);
                for(var j = 1; j < rounds; j++){
                    var pole = 'n'; var novy = '';
                    for(var k = 0; k < pocetBitov; k+=4){//substitucia
                        pole += hexToBin(subst[hexToDec(binToHex( zt.substring(k,k+4)))]);
                    }
                    for(var k = 1; k <= pocetBitov; k++){//permutacia
                        novy += pole[parseInt(perm[k])];
                    }
                    zt = xor(novy,subkeys[j]);//xor
                }
                pole = '';
                for(var k = 0; k < pocetBitov; k+=4){//posledna substitucia
                    pole += hexToBin(subst[hexToDec(binToHex( zt.substring(k,k+4)))]);
                }
                var ztext = xor(pole,last_subkey);//posledny xor
                $('#ciphertext ul').append('<li id="zt'+i+'">'+ztext+'</li>');
            }
        }
    });
});

//32 bit master key, vzdy 16 bit od zaciatku a posun 4 bity doprava
function spnKeySchedule(masterKey,rounds){
    var subkeys = new Array();
    for(var i = 0; i <= rounds; i++){
        subkeys[i] = masterKey.substring(0+i*4,16+i*4);
    }
    return subkeys;
}

////80 bit master key...
function presentKeySchedule(masterKey, rounds){
    var subkeys = new Array();
    var mkey = masterKey;
    var temp = '';    
    var subst = '';
    for(var i = 0; i < 16; i++){
        subst += $('#subst'+i).val();
    }
    subkeys[0] = masterKey.substring(0,64);
    for(var i = 1; i <= rounds; i++){
        mkey = mkey.substring(61,80) + '' + masterKey.substring(0,61);
        temp = mkey.substring(0,4);
        temp = hexToBin(subst[hexToDec(binToHex(temp))]);
        mkey = temp + '' + mkey.substring(4,80);
        var r = parseInt(i).toString(2);
        var tmp = 5 - r.length;
        if (r.length != 5) for(var j = 0; j < tmp; j++)r = '0'+r;
        //ina akcia s temp premennou
        temp = xor(r,mkey.substring(60,65));
        mkey = mkey.substring(0,60) + '' + temp + '' + mkey.substring(65,80);
        subkeys[i] = mkey.substring(0,64);
    }
    return subkeys;
}

function generateRandomBits(pocet){
    var subkey = '';
    for(var i = 0; i < pocet; i++){
        var number = Math.round(Math.random());
        subkey += number;
    }
    return subkey;
}

function createPlaintexts(pocet){
    var temp = generateRandomBits(pocet);
    var pocet_vzoriek = $('#pocet_vzoriek').val();
    $('#plaintext ul').append('<li id="ot0">'+temp+'</li>');
    $('#model_sample').append('<option id="ms0">0</option>');
    for(var i = 1; i < pocet_vzoriek; i++){//10000
        var prev = $('#ot'+(i-1)).val();
        temp = ''; temp = generateRandomBits(pocet);
        if (temp != prev) var novy = temp;
        $('#plaintext ul').append('<li id="ot'+i+'">'+novy+'</li>');
        $('#model_sample').append('<option id="ms'+i+'">'+i+'</option>');
    }
}

function xor(a,b){
	var str = '';
	if (a.length != b.length) return false;
	else{
		for(var i = 0; i < a.length; i++){
			str+=a[i]^b[i];
		}
	}
	return str;
}

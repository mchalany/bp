var worker;

$(window).ready(function(){
    var pole = new Array();
    var bity = new Array();
    var pozicie = new Array();
    var kombinacie = new Array();
    var ot_pos//pozicie oznacenych bitov OT
    
    //vypise target partial bits of last subkey
    //vygeneruje jednotlive kombinacie pre zvolene bloky podkluca, ktore obsahuju target partial bits
    //xoruje kazdu kombinaciu s ot a zt a ak nula tak zvysi pocitadlo
    //na konci pocitadlo podelene poctom sample zt-ot = prakticka vychylka
    //ktora vychylka najvacsia, spravny podkluc
    $('#applylc').click(function(){
        
        $('.td-target-subkey').each(function(){
            var b = $(this).text();
            var r = $(this).text()[1];
            var string = '';
            b = parseInt(b.split(',')[1]);
            
            if (pozicie.indexOf(b) < 0)
                pozicie = pozicie.concat(positionPartialBits(b));
            
            for(var i = 0; i < pozicie.length; i++){
                if (b == pozicie[i]) bity.push(i);
            }
                        
            string = targetPartialArray(b,r);
            if (pole.indexOf(string) < 0){
                pole.push(string);
                $('#target-bits').append('<p class="p-target-bit">'+string+'</p>');
            }
        });
        
        var neoznacene = $('#last_sub').text().split(' ')[1];
        
        //vygeneruj kombinacie a zapis do tabulky
        kombinacie = vygenerujKombinacie(pole.length);
        for(var i = 0; i < kombinacie.length; i++){
            $('#model_comb').append('<option id="mk'+i+'">'+kombinacie[i]+'</option>');
            $('#fin-table').append('<tr><td id="k'+i+'">'+kombinacie[i]+'</td><td class="b" id="b'+i+'">?</td></tr>');
        }
    });
    
    $('#start').click(function(){
//        $('#menu-left').show();
        $('#menu-button').text('⇐');
        $('#menu-left').animate({
            width:'15%'
        },200);
        $('#menu-left ul').show();
        var subst = '';
        for(var i = 0; i < 16; i++){
            subst += $('#subst'+i).val();
        }
        var pocet_vzoriek = $('#pocet_vzoriek').val();
        worker = new Worker('./js/biases.js');
        worker.onmessage = spracujWorkerMessage;
        worker.onerror = spracujWorkerError;
        var ot_array = new Array();
        var zt_array = new Array();
        ot_pos = findPlaintextPos();
        for(var j = 0; j < pocet_vzoriek; j++){
            zt_array[j] = getSampleBits($('#zt'+j).text(),pozicie);
            ot_array[j] = getSampleBits($('#ot'+j).text(),ot_pos);
        }
        worker.postMessage( { ot: ot_array , zt: zt_array, bity: bity, subst: subst, komb: kombinacie, pocet:pocet_vzoriek } );
    });
    
    $('#stop').click(function(){
        zastavVypocet();
    });
    
    $('#best').click(function(){
        var i =0;
        var best = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]];
        var pocet = $('.b').last().attr('id');
        pocet = parseInt(pocet.substr(1,pocet.length),10);
        for(var i = 0; i <= pocet; i++){
            var novy = [$('#b'+i).text(),$('#k'+i).text()];
//            if (parseInt(novy,10) > best) best = novy;
            for(var j = 0; j < 10; j++){
                if (novy[0] > best[j][0]){
                    best.splice(j,0,novy);
                    if (best.length > 10) best.pop();
                    break;
                }
            }
        }
        for(var i = 0; i < best.length; i++){
            $('#b-table').append('<tr><td id="k-best-'+i+'">'+best[i][1]+'</td><td id="b-best-'+i+'">'+best[i][0]+'</td></tr>');
        }
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);
    });
    
    $('#button-graph-table').click(function(){
        $('#best-table').toggle();
        $('#best-graph').toggle();
    });
});

function spracujWorkerMessage(event){
	var message = event.data;
	
	if (message.messageType == "VypisBiasu"){
		var bias = message.data;
		for(var i = 0; i < bias.length; i++){
            $('#b'+i).text(bias[i]);
        }
	}
	else if(message.messageType == "VypisStavu"){
		$('#idStav').text(message.data + "% done ...");
	}
}

function spracujWorkerError(error){
	$('#idStav').text(error.message);
}

function zastavVypocet(){
	worker.terminate();
	worker = null;
	$('#idStav').text("");
}


function drawBasic(){
    
    var data = new google.visualization.DataTable();
    
    data.addColumn('number','Bias');
    data.addColumn('number','Combination');
    var pole = new Array();
    for(var i = 0; i < 10; i++){
        var val1 = parseInt($('#k-best-'+i).text(),16);
        var val2 = parseFloat($('#b-best-'+i).text());
        pole.push([val1,val2]);
    }
    data.addRows(pole);

      var options = {
        title: 'Highest biases with respective combinations',
        chartArea: {width: '50%'},
        vAxis: {
          title: 'Combination'
        },
        hAxis: {
          title: 'Bias'
        }
      };

      var chart = new google.visualization.BarChart(document.getElementById('best-graph'));

      chart.draw(data, options);
    }

function findPlaintextPos(){
    var pos = new Array();
    $('.td-active-subkey:contains(K1)').each(function(){  
//        console.log($(this).text());
        pos.push($(this).text().split(',')[1]);
    });
    return pos;
}


function getSampleBits(text, pos){
    var sample = '';
//    console.log(text);
//    console.log(pos);
    for(var i = 0; i < pos.length; i++){
         sample += text[pos[i]-1];
    }
//    console.log(sample);
    return sample;
}

function positionPartialBits(b){
    var pole = new Array();
    if (b % 4 == 1) {
        pole[0] = b; pole[1] = (b+1); pole[2] = (b+2); pole[3] = (b+3);
    }
    else if (b % 4 == 2) {
        pole[0] = b-1; pole[1] = b; pole[2] = (b+1); pole[3] = (b+2);
    }
    else if (b % 4 == 3) {
        pole[0] = b-2; pole[1] = (b-1); pole[2] = b; pole[3] = (b+1);
    }
    else if (b % 4 == 0) {
        pole[0] = b-3; pole[1] = (b-2); pole[2] = (b-1); pole[3] = b;
    }
    return pole;
}

//VYTVORIT KOMBINACIE HEX HODNOT OZNACENYCH SUBKEYOV PODLA POCTU OZNACENYCH upravit
function vygenerujKombinacie(p){
    var pole = new Array();
    var komb = '0123456789abcdef';
    if (p == 1){
        for(var i = 0; i < 16; i++){
            var string = komb[i];
            pole.push(string);
        }
    }
    else if (p == 2){
        for(var i = 0; i < 16; i++){
            for(var j = 0; j < 16; j++){
                var string = komb[i]+''+komb[j];
                pole.push(string);
            }
        }
    }
    else if (p == 3){
        for(var i = 0; i < 16; i++){
            for(var j = 0; j < 16; j++){
                for(var k = 0; k < 16; k++){
                    var string = komb[i]+''+komb[j]+''+komb[k];
                    pole.push(string);
                }
            }
        }
    }
    else if (p == 4){
        for(var i = 0; i < 16; i++){
            for(var j = 0; j < 16; j++){
                for(var k = 0; k < 16; k++){
                    for(var l = 0; l < 16; l++){
                        var string = komb[i]+''+komb[j]+''+komb[k]+''+komb[l];
                        pole.push(string);
                    }
                }
            }
        }
    }//bud dorobit takto pre 5,6,7 a 8 blokov ale vymysliet nejak vseobecne
    return pole;
}

function targetPartialArray(b,r){
    var string = '';
    if (b % 4 == 1) {
        string = '[K'+r+','+(b)+'; K'+r+','+(b+1)+'; K'+r+','+(b+2)+'; K'+r+','+(b+3)+']';
    }
    else if (b % 4 == 2) {
        string = '[K'+r+','+(b-1)+'; K'+r+','+b+'; K'+r+','+(b+1)+'; K'+r+','+(b+2)+']';
    }
    else if (b % 4 == 3) {
        string = '[K'+r+','+(b-2)+'; K'+r+','+(b-1)+'; K'+r+','+b+'; K'+r+','+(b+1)+']';
    }
    else if (b % 4 == 0) {
        string = '[K'+r+','+(b-3)+'; K'+r+','+(b-2)+'; K'+r+','+(b-1)+'; K'+r+','+b+']';
    }
    return string;
}

//function positionPartialBits(b){
//    var string = '';
//   if (b % 4 == 1) {
//        string = b +','+(b+1)+'; K'+r+','+(b+2)+'; K'+r+','+(b+3)+']';
//    }
//    else if (b % 4 == 2) {
//        string = '[K'+r+','+(b-1)+'; K'+r+','+b+'; K'+r+','+(b+1)+'; K'+r+','+(b+2)+']';
//    }
//    else if (b % 4 == 3) {
//        string = '[K'+r+','+(b-2)+'; K'+r+','+(b-1)+'; K'+r+','+b+'; K'+r+','+(b+1)+']';
//    }
//    else if (b % 4 == 0) {
//        string = '[K'+r+','+(b-3)+'; K'+r+','+(b-2)+'; K'+r+','+(b-1)+'; K'+r+','+b+']';
//    }
//    return string;
//}

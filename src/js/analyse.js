$(window).ready(function(){
	showSelected();//FKCIA NA TRAJEKTORIE KLIKANIM
});

//FKCIA NA TRAJEKTORIE KLIKANIM
function showSelected(){
	$('.td-block4, .td-block').click(function(){
        var rounds = parseInt(document.getElementById("rounds").value);
        var blocks = parseInt(document.getElementById("blocks").value);
		var current = $(this).parent().closest('div').attr('id');//aktualny div
		var clicked = $(this).text();//text kliknuteho elementu
        var r = parseInt(clicked[1]);//round
		var index = clicked.substring(1,clicked.length);//vsetko okrem X alebo Y (text)
        
        
        if ($(this).hasClass('td-inactive')){
            $(this).addClass('td-active')
                   .removeClass('td-inactive');
                if (clicked[0] == 'X'){//AK SOM KLIKOL NA BLOK S X TAK ZAKTIVUJE PREDOSLY SUBKEY a oznaci predosla line
                    $("#"+current+" td")
                        .filter(function() { return $.text([this]) == 'K'+index+''; })
                        .addClass('td-active-subkey');
                    //oznacenie doplnujucich ciar
                    if (index[0] == 1){
                        var t = index.substring(1,index.length);
                        $('#'+current+' .svg1').find('line')
                            .filter(function() { return $(this).attr('class') == 'line1'+t }).css('stroke','green');
                        $('#'+current+' .svg0').find('line')
                            .filter(function() { return $(this).attr('class') == 'line0'+t }).css('stroke','green');
                    }
                }
                if (clicked[0] == 'Y'){
                        var temp = clicked.split(',')[1];
                        var perm = ["null"];
                        for(var i = 1; i <= blocks*4; i++){                            perm.push(document.getElementById("perm"+i+"").value);
                        }
                    
                    var temp = perm[temp];
                    
                    if ((r+1) != rounds){
                        //oznacenie X bloku
                        $('#'+current+" td")
                            .filter(function() { return $.text([this]) == 'X'+(r+1)+','+temp; })
                            .addClass('td-active');       
                        //oznacenie Y bloku
                        $("#"+current+" td")
                            .filter(function() { return $.text([this]) == 'K'+(r+1)+','+temp; })
                            .addClass('td-active-subkey');
                        //oznacenie ciary, zobrazujucej permutaciu
                        //kedze z-index podla toho ktora bola skor nakreslena tak nakreslit este raz
                        $('#'+current+" #svg"+r).find('line')
                            .filter(function() { return $(this).attr('id') == 'line'+r+','+temp }).css('stroke','green');
                        var line = $('#'+current+" #svg"+r).find('line')
                            .filter(function() { return $(this).attr('id') == 'line'+r+','+temp });
                        $('#svg'+r).append(line);
                        $('#'+current+' .svg'+(r+1)).find('line')
                            .filter(function() { return $(this).attr('class') == 'line'+(r+1)+','+temp }).css('stroke','green');
                    }
                    else{
                        $("#"+current+" td")
                            .filter(function() { return $.text([this]) == 'K'+(r+1)+','+temp+''; })
                            .addClass('td-active-subkey');
                        $("#"+current+" td")
                            .filter(function() { return $.text([this]) == 'K'+(r+2)+','+temp+''; })
                            .addClass('td-target-subkey');
                        $('#'+current+" #svg"+r).find('line')
                            .filter(function() { return $(this).attr('id') == 'line'+r+','+temp }).css('stroke','green');
                        var line = $('#'+current+" #svg"+r).find('line')
                            .filter(function() { return $(this).attr('id') == 'line'+r+','+temp });
                        $('#svg'+r).append(line);
                    }
                }
        }
    });
}      

//VYTVORENIE VYRAZOV PRE JEDNOTLIVE S-BOXy - used: MENU2
//POSTUP FUNKCIE:
//1. VYTVOR POLE OZNACENYCH ELEMENTOV ( getSelectedBoxes() )
//2. PRE KAZDY VYRAZ VYPOCTIAJ PRAVDEPODOBNOST A BIAS ( getProbability(), getBias() )
//3. KAZDY NOVOZAPISANY VYRAZ ZAPOCITAJ DO CELKOVEJ ODCHYLKY A PRAVDEPODOBNOSTI ( pillingUpLemma() )
//4. NA ZAKLADE OZNACENYCH TD-WAY A TD-ACTIVE PRVEHO KOLA VYTVOR A ZAPIS LINEARNY VYRAZ A TAKTIEZ SUMU KLUCOV
function createExpressions(current, oznacene){
	var rounds = parseInt(document.getElementById("rounds").value);
	var biasy = new Array();
	var input = new Array();
	var plaintext = new Array();
	
	var subkeys = new Array();
	$('.td-active-subkey').each(function(){
		subkeys.push($(this).text());
	});
	$('#sum_subkeys').text('∑k = '+subkeys.join(" ⊕ "));

	for(var i = 1; i <= oznacene.length; i++){
        var currentExp = $("#expression"+i+"").text();
        if (currentExp != oznacene[i-1]){
            $("#expression"+i+"").text(oznacene[i-1].join(" ⊕ "));
            var prob = getProbability(oznacene[i-1]);
            $("#bias"+i+"").text('probability = '+prob+'/16, bias = '+getBias(prob)+'/16');
        }
		biasy.push(getBias(prob)/16);
	}
    $('#upper_prob1').text((pillingUpLemma(biasy)+0.5)*32+'/32');
    $('#upper_prob2').text((0.5-pillingUpLemma(biasy))*32+'/32');
    $('#upper_bias').text(pillingUpLemma(biasy));
    
//	for(var i = 1; i <= 4; i++){
    $('#'+current).find('.td-target-subkey').each(function(){
        input.push($(this).text());
    });
    $('#'+current).find('.td-active-subkey:contains("K1,")').each(function(){
        plaintext.push($(this).text());	
    });
//	}
	for(var i =0; i < input.length; i++){
		input[i] = input[i].split('');
		input[i][0] = 'U';
		input[i] = input[i].join(''); 
	}
	for(var i =0; i < plaintext.length; i++){
		plaintext[i] = 'P'+plaintext[i].split(',')[1];
	}
	$('p#linear_expression').text(input.join(" ⊕ ")+' ⊕ '+plaintext.join(" ⊕ "));
}

//VRAT V 2D POLI JEDNOTLIVE OZNACENE S-BOXy -> used: createExpressions(), vystup premenna oznacene!!!
function getSelectedBoxes(current){
	var rounds = parseInt(document.getElementById("rounds").value);
    var blocks = parseInt(document.getElementById("blocks").value);
	var result = new Array();
	var selected = new Array();
	$('#'+current).find('.td-active').each(function(){
		selected.push($(this).text());
	});
	
	var position = ['null'];
	for(var i = 1; i <= rounds;i++){
		position[i] = new Array();
        position[i][0] = 'null';
		for(var j = 1; j <= blocks; j++){
			position[i][j] = new Array();
		}
	}
    
    for(var j = 0; j < selected.length; j++){
        var temp = selected[j];
        var pos = selected[j].split(',');
        var r = parseInt(pos[0].substring(1,pos[0].length));
        var b = Math.ceil(parseInt(pos[1]) / 4);
        position[r][b].push(temp);
    }
    
	for(var i = 1; i <= rounds;i++){
		for(var j = 1; j <= blocks; j++){
			if (position[i][j] != '0' && position[i][j].length > 0)
				result.push(position[i][j]);
		}
	}
    
	return result;
}

function getBias(prob){
	return prob - 8;
}

function pillingUpLemma(b){
	var result = Math.pow(2,b.length-1);
	for(var i = 0; i < b.length; i++){
		result*=b[i];
	}
	return result;
}

//NAJSKOR PREMEN ZOBRATE TEXTY Z OZNACENYCH A KAZDU PREMEN NA X1,X2 a pod.
//DOSTAN PRAVDEPODOBNOST ZE EXPRESSION HOLDS -> used: createExpressions()
function getProbability(exp){
	var temp = '';
	for(var i = 0; i < exp.length; i++){
		temp = getTableElement(exp[i]);
		exp[i] = temp;
	}
	
	var lookupTable = {
		'X1': '0', 'X2': '1', 'X3': '2', 'X4': '3', 
		'Y1': '4', 'Y2': '5', 'Y3': '6', 'Y4': '7'
	}; 
	
	var table = new Array();
	table = getApproximations();
	
	var xorArray = new Array;
	for(var k = 0; k < exp.length; k++){
		xorArray.push(table[lookupTable[exp[k]]]);
	}
	var count = 0;
	for(var i = 0; i < 16; i++){
		var result = xorArray[0][i];
		for(var j = 1; j < xorArray.length; j++){
			result = xor(result,xorArray[j][i]);
		}
		if(result == 0){
			count++;
		}
	}
	
	return count;
}

//ZAMENA ELEMENTOV TYPU napr. X1,7 NA X3 -> used: getProbability()
function getTableElement(e){
    var a = e.split(',')[1];
	if(e[0] == 'X'){
            if ( a % 4 == 1 ) e = 'X1';
            else if ( a % 4 == 2 ) e = 'X2';
            else if ( a % 4 == 3 ) e = 'X3';
            else if ( a % 4 == 0 ) e = 'X4';
	}
	else if(e[0] == 'Y'){
            if ( a % 4 == 1 ) e = 'Y1';
            else if ( a % 4 == 2 ) e = 'Y2';
            else if ( a % 4 == 3 ) e = 'Y3';
            else if ( a % 4 == 0 ) e = 'Y4';
	}
	return e;
}

//SPRAV TABULKU LINEARNYCH APROXIMACII PRE DANU SUBSTITUCIU -> used: MENU4
function getApproximations(){
	var table = new Array();
	for(var i = 1; i <= 4; i++){
		var polex = new Array;
		$('.lin-x'+i+'').each(function(){
			polex.push($(this).text());
		});
		table.push(polex);
	}	
	for(var i = 1; i <= 4; i++){
		var poley = new Array;
		$('.lin-y'+i+'').each(function(){
			poley.push($(this).text());
		});
		table.push(poley);
	}
//    console.log(table);
	return table;
}
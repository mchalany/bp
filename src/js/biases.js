onmessage = function(event){
	var current_bias = fillFinalTable(event.data.ot, event.data.zt, event.data.bity, event.data.subst, event.data.komb, event.data.pocet);
	postMessage({messageType: "VypisBiasu", data: current_bias});
};

function fillFinalTable(ot,zt,bity,subst,komb,pocet){
    var inv_subst = getInvSubst(subst);
	var poslednyStav = 0;
    var bias = new Array();
    for(var i = 0; i < komb.length; i++){
        var x = 0;
        for(var j = 0; j < pocet; j++){
            if (calculateBias(ot[j],zt[j],komb[i],inv_subst,bity)) x++;
        }
        var aktualnyStav = Math.round(i/(komb.length)*100);
        if(aktualnyStav != poslednyStav){
            postMessage({messageType: "VypisStavu", data: aktualnyStav});
            poslednyStav = aktualnyStav;
        }
        bias.push(Math.abs((x - (pocet / 2)) / pocet));
    }
    return bias;
}

function getInvSubst(subst){
    var inv = '';
    for(var i = 0; i < subst.length; i++){
        inv += subst.indexOf(i.toString(16)).toString(16);
    }
    return inv;
}

function calculateBias(o, z, komb, subst, bity){
    var k = hexToBin(komb);
    var temp = xor(z,k);
    //SUBSTITUCIA az potom XOR jednotlivych bitov s danymi bitmi OT
    var str = '';
    for(var k = 0; k < temp.length; k+=4){
        str += hexToBin(subst[hexToDec(binToHex( temp.substring(k,k+4)))]);
    }
    var n = '';
    for(var i = 0; i < bity.length; i++){
        n += str[bity[i]];
    }
    var t = n + '' + o;
    while(t.length > 1){
        var result = xor(t[0],t[1]);
        var tmp = t.substring(1,t.length);
        tmp = result+ '' + tmp.substring(1,tmp.length);
        t = tmp;
    }
    if (t == 1) return 0;
    else return 1;
}

function hexToBin(s){
	var ret = '';
	var lookupTable = {
		'0': '0000', '1': '0001', '2': '0010', '3': '0011', '4': '0100',
		'5': '0101', '6': '0110', '7': '0111', '8': '1000', '9': '1001',
		'a': '1010', 'b': '1011', 'c': '1100', 'd': '1101',
		'e': '1110', 'f': '1111',
	};
	for (var i = 0; i < s.length; i += 1) {
		if (lookupTable.hasOwnProperty(s[i])) {
			ret += lookupTable[s[i]];
		} else {
			return { valid: false };
		}
	}
	return ret;
}

function hexToDec(hex){
    return parseInt(hex,16);
}

function xor(a,b){
	var str = '';
	if (a.length != b.length) return false;
	else{
		for(var i = 0; i < a.length; i++){
			str+=a[i]^b[i];
		}
	}
	return str;
}

function binToHex(bin){
	var ret = '';
	var lookupTable = {
		'0000': '0', '0001': '1', '0010': '2', '0011': '3', '0100': '4',
		'0101': '5', '0110': '6', '0111': '7', '1000': '8', '1001': '9',
		'1010': 'a', '1011': 'b', '1100': 'c', '1101': 'd',
		'1110': 'e', '1111': 'f'
	};
	if (lookupTable.hasOwnProperty(bin)) {
		return lookupTable[bin];
	} else {
		return { valid: false };
	}
	return ret;
}
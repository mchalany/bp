<?php

    $rounds = $_POST['kola'];
	$p = $_POST['permutacia'];
    $blocks = $_POST['bloky'];

    $permut = array('null');
    
    $colspan = $blocks*5-1;

    $permut = [ "1" => "5", "2" => "15", "3" => "25", "4" => "35",
				"5" => "55", "6" => "65", "7" => "75", "8" => "85",
				"9" => "105", "10" => "115", "11" => "125", "12" => "135",
				"13" => "155", "14" => "165", "15" => "175", "16" => "185",
                "17" => "205", "18" => "215", "19" => "225", "20" => "235",
				"21" => "255", "22" => "265", "23" => "275", "24" => "285",
				"25" => "305", "26" => "315", "27" => "325", "28" => "335",
				"29" => "355", "30" => "365", "31" => "375", "32" => "385",
                "33" => "405", "34" => "415", "35" => "425", "36" => "435",
				"37" => "455", "38" => "465", "39" => "475", "40" => "485",
				"41" => "505", "42" => "515", "43" => "525", "44" => "535",
				"45" => "555", "46" => "565", "47" => "575", "48" => "585",
                "49" => "605", "50" => "615", "51" => "625", "52" => "635",
				"53" => "655", "54" => "665", "55" => "675", "56" => "685",
				"57" => "705", "58" => "715", "59" => "725", "60" => "735",
				"61" => "755", "62" => "765", "63" => "775", "64" => "785"
              ];

    echo "<table id='table-cipher' align='center''>
			<tr>";
            for($i = 1; $i <= sizeof($p)-1; $i+=4){
                echo "<td class='td-bin-p'>P".$i."</td>
                <td class='td-bin-p'>P".($i+1)."</td>
                <td class='td-bin-p'>P".($i+2)."</td>
                <td class='td-bin-p'>P".($i+3)."</td>";
				if (($i+3) != sizeof($p)-1) 
                    echo "<td class='td-empty'></td>";
            }
            echo "</tr>
			<tr class='tr-empty'>
				<td colspan='".$colspan."'>
					<svg class='svg0 svg-s'>
					</svg>
				</td>
			</tr><tr>";
		for($i = 1; $i <= $rounds - 1; $i++){
            for($j = 1; $j <= sizeof($p)-1; $j+=4){
                echo "<td class='td-subkey'>K".$i.",".$j."</td>
                <td class='td-subkey'>K".$i.",".($j+1)."</td>
                <td class='td-subkey'>K".$i.",".($j+2)."</td>
                <td class='td-subkey'>K".$i.",".($j+3)."</td>";
				if (($j+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
            }
            echo "</tr>
				<tr class='tr-empty'>
				<td colspan='".$colspan."'>
					<svg class='svg".$i." svg-s'>
					</svg>
				</td>
			</tr><tr>";
            for($j = 1; $j <= sizeof($p)-1; $j+=4){
                echo "<td class='td-block td-inactive'>X".$i.",".$j."</td><td class='td-block td-inactive'>X".$i.",".($j+1)."</td>
                <td class='td-block td-inactive'>X".$i.",".($j+2)."</td>
                <td class='td-block td-inactive'>X".$i.",".($j+3)."</td>";
                if (($j+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
            }
				echo "</tr><tr>";
            for($j = 1; $j <= sizeof($p)-1; $j+=4){
                echo "<td class='td-block td-inactive'>Y".$i.",".$j."</td><td class='td-block td-inactive'>Y".$i.",".($j+1)."</td><td class='td-block td-inactive'>Y".$i.",".($j+2)."</td><td class='td-block td-inactive'>Y".$i.",".($j+3)."</td>";
                if (($j+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
            }
            echo "</tr>
                <tr class='tr-empty'><td colspan='".$colspan."'>
				<svg id=svg".$i." class='svg-p'>";
            for($j = 1; $j <= sizeof($p)-1; $j++){
                echo "<line id='line".$i.",".$p[$j]."' x1='".$permut[$j]."' y1='0' x2='".$permut[$p[$j]]."' y2='80' style='stroke:black;stroke-width:2'/>";
            }
            echo "</svg></td></tr><tr>";
		}
        
        //POSLEDNE KOLO: PODKLUC, X a Y tdcka v SBOXE, posledny PODKLUC a ZT
        for($j = 1; $j <= sizeof($p)-1; $j+=4){
            echo "<td class='td-subkey'>K".$rounds.",".$j."</td>
            <td class='td-subkey'>K".$rounds.",".($j+1)."</td>
            <td class='td-subkey'>K".$rounds.",".($j+2)."</td>
            <td class='td-subkey'>K".$rounds.",".($j+3)."</td>";
            if (($j+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
        }
        echo "</tr>
            <tr class='tr-empty'>
            <td colspan='".$colspan."'>
                <svg class='svg".($rounds)." svg-s'>
                </svg>
            </td>
        </tr><tr>";
        for($j = 1; $j <= sizeof($p)-1; $j+=4){
            echo "<td class='td-block'>X".$rounds.",".$j."</td>
            <td class='td-block'>X".$rounds.",".($j+1)."</td>
            <td class='td-block'>X".$rounds.",".($j+2)."</td>
            <td class='td-block'>X".$rounds.",".($j+3)."</td>";
            if (($j+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
        }
            echo "</tr><tr>";
        for($j = 1; $j <= sizeof($p)-1; $j+=4){
            echo "<td class='td-block'>Y".$rounds.",".$j."</td>
            <td class='td-block'>Y".$rounds.",".($j+1)."</td>
            <td class='td-block'>Y".$rounds.",".($j+2)."</td>
            <td class='td-block'>Y".$rounds.",".($j+3)."</td>";
            if (($j+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
        }
        echo "</tr><tr><tr class='tr-empty'>
                <td colspan='".$colspan."'>
				    <svg class='svg".($rounds+1)." svg-s'></svg>
                </td></tr><tr>";
        for($j = 1; $j <= sizeof($p)-1; $j+=4){
           echo "<td class='td-subkey'>K".($rounds+1).",".$j."</td><td class='td-subkey'>K".($rounds+1).",".($j+1)."</td><td class='td-subkey'>K".($rounds+1).",".($j+2)."</td><td class='td-subkey'>K".($rounds+1).",".($j+3)."</td>
            <td class='td-empty'></td>";
        }
        echo "</tr>
			<tr class='tr-empty'>
				<td colspan='".$colspan."'>
					<svg class='svg".($rounds+2)." svg-s'>
					</svg>
				</td>
			</tr>
			<tr>";
			for($i = 0; $i < sizeof($p)-1; $i+=4){
				echo "<td class='td-bin-p>C".$i."</td>";
				echo "<td class='td-bin-p>C".($i+1)."</td>";
				echo "<td class='td-bin-p>C".($i+2)."</td>";
				echo "<td class='td-bin-p>C".($i+3)."</td>";
                if (($i+3) != sizeof($p)-1) echo "<td class='td-empty'></td>";
			}
			echo "</tr>
		</table>";
	
	echo "<script type='text/javascript' src='./js/analyse.js'></script>
		<script>";
			for($j = 0; $j < $rounds+3; $j++){
                //TOTO SU VYPLNUJUCE CIARY
				for($i = 1; $i <= sizeof($p)-1; $i++){
					echo "var lineElement = document.createElementNS('http://www.w3.org/2000/svg', 'line');
						lineElement.setAttribute('x1', ".$permut[$i].");
						lineElement.setAttribute('x2', ".$permut[$i].");
						lineElement.setAttribute('y1', '0');
						lineElement.setAttribute('y2', '15');			 
						lineElement.setAttribute('stroke', 'black');
						lineElement.setAttribute('stroke-width', '2');
						lineElement.setAttribute('class', 'line".$j.",".$i."');
						$('.svg".$j."').append(lineElement);
					";
				}
			}	
		echo 	"</script>";

?>

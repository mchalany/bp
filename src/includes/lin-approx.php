<?php
	$sub = $_POST['substitucia'];
	$x1 = array("0","0","0","0","0","0","0","0","1","1","1","1","1","1","1","1");
	$x2 = array("0","0","0","0","1","1","1","1","0","0","0","0","1","1","1","1");
	$x3 = array("0","0","1","1","0","0","1","1","0","0","1","1","0","0","1","1");
	$x4 = array("0","1","0","1","0","1","0","1","0","1","0","1","0","1","0","1");
	
	$y1 = array();
	$y2 = array();
	$y3 = array();
	$y4 = array();
	
	echo "<h2>Linear approximations for specified substitution</h2>
	<table id='table-approx'>
		<thead>
			<th>X1</th><th>X2</th><th>X3</th><th>X4</th>
			<th>Y1</th><th>Y2</th><th>Y3</th><th>Y4</th>
		</thead>";
		for($i = 0; $i < 16; $i++){
			$temp = base_convert($sub[$i],16,2);//z hex do bin
			if(strlen($temp) == 3){
				array_push($y1,"0");
				array_push($y2,$temp[0]);
				array_push($y3,$temp[1]);
				array_push($y4,$temp[2]);
			}
			else if(strlen($temp) == 2){
				array_push($y1,"0");
				array_push($y2,"0");
				array_push($y3,$temp[0]);
				array_push($y4,$temp[1]);
			}
			else if(strlen($temp) == 1){
				array_push($y1,"0");
				array_push($y2,"0");
				array_push($y3,"0");
				array_push($y4,$temp[0]);
			}
			else{
				array_push($y1,$temp[0]);				
				array_push($y2,$temp[1]);
				array_push($y3,$temp[2]);
				array_push($y4,$temp[3]);
			}
			
			echo "<tr align='center'>
				<td class='lin-x1'>".$x1[$i]."</td>
				<td class='lin-x2'>".$x2[$i]."</td>
				<td class='lin-x3'>".$x3[$i]."</td>
				<td class='lin-x4'>".$x4[$i]."</td>
				
				<td class='lin-y1'>".$y1[$i]."</td>
				<td class='lin-y2'>".$y2[$i]."</td>
				<td class='lin-y3'>".$y3[$i]."</td>
				<td class='lin-y4'>".$y4[$i]."</td>
			</tr>";
		}
	echo "</table>";
?>
<?php
	
	$possible = $_POST['possible'];
	$bias = $_POST['bias'];
	
	echo "<table class='possible' id='possible'>
		<thead style='background-color:white;color:black'>
			<th id='e-sort'>Expression variables</th>
			<th id='b-sort'>Bias</th>
		</thead>";
	for($i = 0; $i < count($possible); $i++){
		echo "<tr>
				<td class='e-poss'>".$possible[$i]."</td>
				<td class='e-bias'>".$bias[$i]."/16</td>
			</tr>";
	}
	echo "</table>";
?>
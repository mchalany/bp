
<?php
    $pocet = $_POST['pocet'];
    echo "<table id='table-params'>
        <tr>
            <td>
                <label>Number of rounds</label>
                <select id='rounds'>
                    <option value='2'>2</option>
                    <option value='3'>3</option>
                    <option value='4'>4</option>
                    <option value='5'>5</option>
                    <option value='6'>6</option>
                    <option value='7'>7</option>
                    <option value='8'>8</option>
                    <option value='9'>9</option>
                </select>
            </td>
            <td style='border-left:1px solid navy'>
                <label>Type the master key (hex):</label><br/>
                <input placeholder='present = 80 bit, SPN = 32 bit' type='text' id='master_key' maxlength='".$pocet."'>
            </td>      
        </tr>";
    
    echo "<td colspan='2' style='border-top:1px solid navy'>
        <label>Substitution:</label><br/>
        <table align='center'><tr>";
        for($i = 0; $i < 16; $i++){
            if ($i > 9) echo "<td>".dechex($i)."</td>";
            else echo "<td>".$i."</td>";
        }
        echo "</tr><tr>";
        for($i = 0; $i < 16; $i++){
            echo "<td><input id='subst".$i."' class='td-subst' maxlength='1' type='text'><br/></td>";
        }
        echo "</tr></table></td>";


    echo "<tr><td colspan='2' style='border-top:1px solid navy' colspan='2'><label>Permutation:</label></tr>
            <tr><td colspan='2'><table id='permtable' align='center'><tr>";
    for($i = 1; $i <= $pocet; $i++){
        echo "<td>".$i."</td>
                <td><input id='perm".$i."' class='td-perm' maxlength='2' type='text'><br/></td>";
        if ($i % 10 == 0) echo "</tr><tr>";
    }
echo "</tr></table></td></tr>";
?>

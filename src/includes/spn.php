<?php
	$rounds = $_POST['kola'];
	$p = $_POST['permutacia'];
	
	$permut = [ "1" => "15",
				"2" => "50",
				"3" => "85",
				"4" => "120",
				"5" => "170",
				"6" => "205",
				"7" => "240",
				"8" => "275",
				"9" => "325",
				"10" => "360",
				"11" => "395",
				"12" => "430",
				"13" => "480",
				"14" => "515",
				"15" => "550",
				"16" => "585"
				];
	
	echo "<table id='table4' align='center' class='table-block4'>
			<tr>
				<td class='td-bin'>P1</td><td class='td-bin'>P2</td><td class='td-bin'>P3</td><td class='td-bin'>P4</td>
				<td class='td-empty4'></td>
				<td class='td-bin'>P5</td><td class='td-bin'>P6</td><td class='td-bin'>P7</td><td class='td-bin'>P8</td>
				<td class='td-empty4'></td>
				<td class='td-bin'>P9</td><td class='td-bin'>P10</td><td class='td-bin'>P11</td><td class='td-bin'>P12</td>
				<td class='td-empty4'></td>
				<td class='td-bin'>P13</td><td class='td-bin'>P14</td><td class='td-bin'>P15</td><td class='td-bin'>P16</td>
				<td></td>
				<td></td>
			</tr>
			<tr class='tr-empty'>
				<td colspan='19'>
					<svg class='svg0' width='600px' height='15px'>
					</svg>
				</td>
				<td></td>
			</tr>";
		for($i = 1; $i <= $rounds - 1; $i++){
			echo "				
				<tr>
					<td class='td-subkey'>K".$i.",1</td><td class='td-subkey'>K".$i.",2</td><td class='td-subkey'>K".$i.",3</td><td class='td-subkey'>K".$i.",4</td>
					<td class='td-empty4'></td>
					<td class='td-subkey'>K".$i.",5</td><td class='td-subkey'>K".$i.",6</td><td class='td-subkey'>K".$i.",7</td><td class='td-subkey'>K".$i.",8</td>
					<td class='td-empty4'></td>
					<td class='td-subkey'>K".$i.",9</td><td class='td-subkey'>K".$i.",10</td><td class='td-subkey'>K".$i.",11</td><td class='td-subkey'>K".$i.",12</td>
					<td class='td-empty4'></td>
					<td class='td-subkey'>K".$i.",13</td><td class='td-subkey'>K".$i.",14</td><td class='td-subkey'>K".$i.",15</td><td class='td-subkey'>K".$i.",16</td>
					<td></td><td></td>
				</tr>
				<tr class='tr-empty'>
				<td colspan='19'>
					<svg class='svg".($i)."' width='600px' height='15px'>
					</svg>
				</td>
				<td></td>
			</tr>
				<tr>
					<td class='td-block4 td-inactive'>X".$i.",1</td><td class='td-block4 td-inactive'>X".$i.",2</td><td class='td-block4 td-inactive'>X".$i.",3</td><td class='td-block4 td-inactive'>X".$i.",4</td>
					<td class='td-empty4'></td>			
					<td class='td-block4 td-inactive'>X".$i.",5</td><td class='td-block4 td-inactive'>X".$i.",6</td><td class='td-block4 td-inactive'>X".$i.",7</td><td class='td-block4 td-inactive'>X".$i.",8</td>
					<td class='td-empty4'></td>			
					<td class='td-block4 td-inactive'>X".$i.",9</td><td class='td-block4 td-inactive'>X".$i.",10</td><td class='td-block4 td-inactive'>X".$i.",11</td><td class='td-block4 td-inactive'>X".$i.",12</td>
					<td class='td-empty4'></td>			
					<td class='td-block4 td-inactive'>X".$i.",13</td><td class='td-block4 td-inactive'>X".$i.",14</td><td class='td-block4 td-inactive'>X".$i.",15</td><td class='td-block4 td-inactive'>X".$i.",16</td>
					<td></td>
					<td>ROUND</td>
				</tr>
				<tr>
					<td class='td-block4 td-inactive'>Y".$i.",1</td><td class='td-block4 td-inactive'>Y".$i.",2</td><td class='td-block4 td-inactive'>Y".$i.",3</td><td class='td-block4 td-inactive'>Y".$i.",4</td>
					<td class='td-empty4'></td>			
					<td class='td-block4 td-inactive'>Y".$i.",5</td><td class='td-block4 td-inactive'>Y".$i.",6</td><td class='td-block4 td-inactive'>Y".$i.",7</td><td class='td-block4 td-inactive'>Y".$i.",8</td>
					<td class='td-empty4'></td>			
					<td class='td-block4 td-inactive'>Y".$i.",9</td><td class='td-block4 td-inactive'>Y".$i.",10</td><td class='td-block4 td-inactive'>Y".$i.",11</td><td class='td-block4 td-inactive'>Y".$i.",12</td>
					<td class='td-empty4'></td>			
					<td class='td-block4 td-inactive'>Y".$i.",13</td><td class='td-block4 td-inactive'>Y".$i.",14</td><td class='td-block4 td-inactive'>Y".$i.",15</td><td class='td-block4 td-inactive'>Y".$i.",16</td>					
					<td></td>
					<td>".$i."</td>
				</tr>
				<tr class='tr-empty'>
					<td colspan='19'>
						<svg id='svg".$i."' width='600px' height='50px'>
							<line id='line".$i.",".$p[1]."' x1='15' y1='0' x2='".$permut[$p[1]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[2]."' x1='50' y1='0' x2='".$permut[$p[2]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[3]."' x1='85' y1='0' x2='".$permut[$p[3]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[4]."' x1='120' y1='0' x2='".$permut[$p[4]]."' y2='50' style='stroke:black;stroke-width:2'/>
							
							<line id='line".$i.",".$p[5]."' x1='170' y1='0' x2='".$permut[$p[5]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[6]."' x1='205' y1='0' x2='".$permut[$p[6]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[7]."' x1='240' y1='0' x2='".$permut[$p[7]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[8]."' x1='275' y1='0' x2='".$permut[$p[8]]."' y2='50' style='stroke:black;stroke-width:2'/>
							
							<line id='line".$i.",".$p[9]."' x1='325' y1='0' x2='".$permut[$p[9]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[10]."' x1='360' y1='0' x2='".$permut[$p[10]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[11]."' x1='395' y1='0' x2='".$permut[$p[11]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[12]."' x1='430' y1='0' x2='".$permut[$p[12]]."' y2='50' style='stroke:black;stroke-width:2'/>
							
							<line id='line".$i.",".$p[13]."' x1='480' y1='0' x2='".$permut[$p[13]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[14]."' x1='515' y1='0' x2='".$permut[$p[14]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[15]."' x1='550' y1='0' x2='".$permut[$p[15]]."' y2='50' style='stroke:black;stroke-width:2'/>
							<line id='line".$i.",".$p[16]."' x1='585' y1='0' x2='".$permut[$p[16]]."' y2='50' style='stroke:black;stroke-width:2'/>
						</svg>
					</td>
					<td></td><td></td>
				</tr>
			";
		}
		echo "
			<tr>
				<td class='td-subkey'>K".$rounds.",1</td><td class='td-subkey'>K".$rounds.",2</td><td class='td-subkey'>K".$rounds.",3</td><td class='td-subkey'>K".$rounds.",4</td>
				<td class='td-empty4'></td>
				<td class='td-subkey'>K".$rounds.",5</td><td class='td-subkey'>K".$rounds.",6</td><td class='td-subkey'>K".$rounds.",7</td><td class='td-subkey'>K".$rounds.",8</td>
				<td class='td-empty4'></td>
				<td class='td-subkey'>K".$rounds.",9</td><td class='td-subkey'>K".$rounds.",10</td><td class='td-subkey'>K".$rounds.",11</td><td class='td-subkey'>K".$rounds.",12</td>
				<td class='td-empty4'></td>
				<td class='td-subkey'>K".$rounds.",13</td><td class='td-subkey'>K".$rounds.",14</td><td class='td-subkey'>K".$rounds.",15</td><td class='td-subkey'>K".$rounds.",16</td>
				<td></td><td></td>
			</tr>
			<tr class='tr-empty'>
				<td colspan='19'>
					<svg class='svg".$rounds."' width='600px' height='15px'>
					</svg>
				</td>
				<td></td><td></td>
			</tr>
			<tr>
				<td class='td-block4'>X".$rounds.",1</td>
                <td class='td-block4'>X".$rounds.",2</td>
                <td class='td-block4'>X".$rounds.",3</td>
                <td class='td-block4'>X".$rounds.",4</td>
				<td class='td-empty4'></td>			
				<td class='td-block4'>X".$rounds.",5</td>
                <td class='td-block4'>X".$rounds.",6</td>
                <td class='td-block4'>X".$rounds.",7</td>
                <td class='td-block4'>X".$rounds.",8</td>
				<td class='td-empty4'></td>			
				<td class='td-block4'>X".$rounds.",9</td>
                <td class='td-block4'>X".$rounds.",10</td>
                <td class='td-block4'>X".$rounds.",11</td>
                <td class='td-block4'>X".$rounds.",12</td>
				<td class='td-empty4'></td>			
				<td class='td-block4'>X".$rounds.",13</td>
                <td class='td-block4'>X".$rounds.",14</td>
                <td class='td-block4'>X".$rounds.",15</td>
                <td class='td-block4'>X".$rounds.",16</td>				
				<td></td>
				<td>ROUND</td>
			</tr>
			<tr>
				<td class='td-block4'>Y".$rounds.",1</td>
                <td class='td-block4'>Y".$rounds.",2</td>
                <td class='td-block4'>Y".$rounds.",3</td>
                <td class='td-block4'>Y".$rounds.",4</td>
				<td class='td-empty4'></td>			
				<td class='td-block4'>Y".$rounds.",5</td>
                <td class='td-block4'>Y".$rounds.",6</td>
                <td class='td-block4'>Y".$rounds.",7</td>
                <td class='td-block4'>Y".$rounds.",8</td>
				<td class='td-empty4'></td>			
				<td class='td-block4'>Y".$rounds.",9</td>
                <td class='td-block4'>Y".$rounds.",10</td>
                <td class='td-block4'>Y".$rounds.",11</td>
                <td class='td-block4'>Y".$rounds.",12</td>
				<td class='td-empty4'></td>			
				<td class='td-block4'>Y".$rounds.",13</td>
                <td class='td-block4'>Y".$rounds.",14</td>
                <td class='td-block4'>Y".$rounds.",15</td>
                <td class='td-block4'>Y".$rounds.",16</td>				
				<td></td>
				<td>".$i."</td>
			</tr>
			<tr class='tr-empty'>
				<td colspan='19'>
					<svg class='svg".($rounds+1)."' width='600px' height='15px'>
					</svg>
				</td>
				<td></td><td></td>
			</tr>
			<tr>
				<td class='td-subkey'>K".($rounds+1).",1</td><td class='td-subkey'>K".($rounds+1).",2</td><td class='td-subkey'>K".($rounds+1).",3</td><td class='td-subkey'>K".($rounds+1).",4</td>
				<td class='td-empty4'></td>
				<td class='td-subkey'>K".($rounds+1).",5</td><td class='td-subkey'>K".($rounds+1).",6</td><td class='td-subkey'>K".($rounds+1).",7</td><td class='td-subkey'>K".($rounds+1).",8</td>
				<td class='td-empty4'></td>
				<td class='td-subkey'>K".($rounds+1).",9</td><td class='td-subkey'>K".($rounds+1).",10</td><td class='td-subkey'>K".($rounds+1).",11</td><td class='td-subkey'>K".($rounds+1).",12</td>
				<td class='td-empty4'></td>
				<td class='td-subkey'>K".($rounds+1).",13</td><td class='td-subkey'>K".($rounds+1).",14</td><td class='td-subkey'>K".($rounds+1).",15</td><td class='td-subkey'>K".($rounds+1).",16</td>
				<td></td><td></td>
			</tr>
			<tr class='tr-empty'>
				<td colspan='19'>
					<svg class='svg".($rounds+2)."' width='600px' height='15px'>
					</svg>
				</td>
				<td></td><td></td>
			</tr>
			<tr>";
			for($i = 0; $i < 16; $i++){
				if($i == 4 || $i == 8 || $i == 12)
					echo "<td class='td-empty4'></td>";
				echo "<td class='td-bin'>C".($i+1)."</td>";
			}
			echo "</tr>
		</table>";
	
	echo "<script type='text/javascript' src='./js/analyse.js'></script>
		<script>	";
			for($j = 0; $j <= $rounds+2; $j++){
				echo "var svgElement = document.getElementsByTagName('svg');";
				for($i = 1; $i <= 16; $i++){
					echo "var lineElement = document.createElementNS('http://www.w3.org/2000/svg', 'line');
						lineElement.setAttribute('x1', ".$permut[$i].");
						lineElement.setAttribute('x2', ".$permut[$i].");
						lineElement.setAttribute('y1', '0');
						lineElement.setAttribute('y2', '15');				 
						lineElement.setAttribute('stroke', 'black');				 
						lineElement.setAttribute('stroke-width', '2');
						lineElement.setAttribute('class', 'line".$j.",".$i."');
						$('.svg".$j."').append(lineElement);
					";
				}
			}	
		echo 	"</script>";
?>
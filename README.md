# README

Web application for Bachelor thesis project - Linear Cryptanalysis of Block Ciphers (2017).

Application consists of:

- dynamically created interactive models of chosen block ciphers
- manual and automatic model analysis for chosen attack
- generating keys, plaintexts and ciphertexts used for attack
- application of known attack on the cipher
- detailed view of the attack with the result

Languages:

- javascript, jquery
- php
- html, css

![bp](./bp.gif)
